import java.io.*;
import java.net.*;
import java.util.*;
import java.io.IOException;
import java.io.File;



public class VacationSalesman {
	Scanner console = new Scanner(System.in);
	// Scanner to read the list of cities
	static final String PUBLIC_API_KEY = "AIzaSyA3gdHsyvOGdQsw7TjGpqPjN7oANsXWE7Q";
	String distance = "";
	int[] distances;
	public VacationSalesman() {

	}
	public void read(String[] cities,int len) {
		
		
		distances = new int[len-1];
		for (int j = 0; j < len-1; j++) {

			BufferedWriter writer = null;
			String line = null;
			try {
				
				String mapsURL = "https://maps.googleapis.com/maps/api/distancematrix/xml?origins=" + cities[j] + "&destinations=" + cities[j+1] + "&key=AIzaSyA3gdHsyvOGdQsw7TjGpqPjN7oANsXWE7Q";
				URL googleMaps = new URL(mapsURL); 
				URLConnection gm = googleMaps.openConnection();
				BufferedReader in = new BufferedReader(
	                                new InputStreamReader(
	                                gm.getInputStream()));
	        	String inputLine;

	        	while ((inputLine = in.readLine()) != null) {
	            	line = line + inputLine;


	            }
	        	in.close();
			} catch (MalformedURLException m) {
				m.printStackTrace();
			} catch (IOException i) {
				i.printStackTrace();
			} 
		int occurrence = 0;
		for (int i = 0; i < line.length(); i++) {
			//Assume second occurrence of <text> is followed by the distance
			if ((line.charAt(i) == 't') && (line.charAt(i+1) == 'e') && (line.charAt(i+2) == 'x') && (line.charAt(i+3) == 't') && (line.charAt(i+4) == '>')) {
				occurrence++;
				if (occurrence > 1) {
					i = i + 5;
					while (line.charAt(i) != ' ' && (line.charAt(i+1) != 'k') && (line.charAt(i+2) != 'm')) {
						if (line.charAt(i) == ',') {

						}
						else {
							distance += line.charAt(i);
						}
						i++;
					}
				}

			}

		}
	distances[j] = Integer.parseInt(distance);

	}
}

	public void printDistance() {
		
		System.out.println(Arrays.toString(distances));
	}

	public static void main(String[] args) {
		VacationSalesman vSalesman = new VacationSalesman();
		String[] cities = new String[30];
		int index = 0;
		if(args.length > 0) {
            File cityfile = new File(args[0]);
            try (BufferedReader br = new BufferedReader(new FileReader(cityfile))) {
    			String city;
    			while ((city = br.readLine()) != null) {
    				cities[index] = city;
    				index++;
    			}
			}
			catch (IOException i) {}
        }		
		vSalesman.read(cities,index);
		vSalesman.printDistance();

	}

}