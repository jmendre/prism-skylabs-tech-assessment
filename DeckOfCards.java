public class DeckOfCards {
		// 1->Diamond, 2->Club, 3->Heart,4->Spade
		//1->A,11->J,12->Q,13->K
		Card[] deck = new Card[52];
		Card[][] card_mapping = new Card[4][13];
		boolean dealt_out = false; 
		int count = 52;
		class Card {
			String suit;
			int number;
			Card(String s, int n) {
				suit = s;
				number = n;
			}
		}
		public void createDeckOfCards() {
			//Creates an ordered deck
			Card card = null;
			int j = 0;
			for (int i = 1; i < 14; i++) {
				if (j == 0) {
					card = new Card("Diamond",i);
				}
				if (j == 1) {
					card = new Card("Club",i);
				}
				if (j == 2) {
					card = new Card("Heart",i);
				}
				if (j == 3) {
					card = new Card("Spade",i);
				}
				deck[j*13+i-1] = card;
				card_mapping[j][i] = card;
				if (i == 13) {
					j++;
				}
			}
		}
		public Card genNextCard() {
			if (count == 0) {
				System.out.println("ALL CARDS ARE DEALT OUT!");
				return null;
			}
			Card card = deck[count-1];
			count--;
			return card;
		}
		public void shuffle() {
			dealt_out = false;
			for (int k = 0; k < 52; k++) {
				deck[k] = null;
			}
			for (int i = 1; i < 13; i++) {
				for (int j = 0; j < 4; j++) {
					int random_slot = 52 + (int)(Math.random() * (52 + 1));

					while (deck[random_slot] != null) {
						random_slot = 52 + (int)(Math.random() * (52 + 1));
					}
					deck[random_slot]  = card_mapping[j][i];
					
				}
			}	
			count = 52;
		}
} 